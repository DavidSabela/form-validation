const controller = require("./controller");

module.exports = app => {
  app.route("/login").post(controller.login);
};

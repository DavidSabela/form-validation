const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const adapter = new FileSync("data/db.json");
const db = low(adapter);

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

// bcrypt.hash("", 10, function(err, hash) {
//   // Store hash in database
//   console.log(hash);
// });

exports.login = (req, res) => {
  const user = req.body;
  var userInDb = db.get("user").value();
  if (user.username === userInDb.username) {
    bcrypt.compare(user.password, userInDb.password).then(function(response) {
      if (response) {
        jwt.sign(
          { username: user.username },
          "secretkey",
          { expiresIn: "3m" },
          (err, token) => {
            res.json({
              token
            });
          }
        );
      } else {
        res.status(400).send("Credentials in not correct");
      }
    });
  } else {
    res.status(400).send("Credentials in not correct");
  }
};

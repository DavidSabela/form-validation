const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const serverless = require("serverless-http");
const app = express();

const router = express.Router();

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, DELETE");
  next();
});

app.use(express.static("public"));
const routes = require("./src/routes");

routes(app);

app.listen(process.env.PORT || 9000, () =>
  console.log("Server started on port 9000")
);

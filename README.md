

## Using Docker

```sh
docker-compose --env-file ./client/env up
```

Verify the deployment by navigating to your client and server address in
your preferred browser.

```sh
client: 127.0.0.1:3000
```
```sh
server: 127.0.0.1:9000
```


## Without Using Docker

#### Start client app:
```sh
cd client
npm install
npm start
```
#### Start server app:
```sh
cd server
npm install
npm start
```
Verify the deployment by navigating to your client and server address in
your preferred browser.

```sh
client: 127.0.0.1:3000
```
```sh
server: 127.0.0.1:9000
```

## Test app
	login: admin@admin.cz
	pass: admin
## License

MIT

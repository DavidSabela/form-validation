import "./App.css";
import "antd/dist/antd.css";
import "react-notifications/lib/notifications.css";
import { NotificationContainer } from "react-notifications";
import LoginForm from "./components/Login";

function App() {
  return (
    <div className="App">
      <LoginForm />
      <NotificationContainer />
    </div>
  );
}

export default App;

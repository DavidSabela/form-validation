import { Form, Input, Button } from "antd";
import axios from "axios";
import Container from "./Container";
import { NotificationManager } from "react-notifications";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 }
};

const LoginForm = () => {
  const onFinish = values => {
    axios({
      method: "post",
      url: process.env.REACT_APP_SERVER + "/login",
      data: {
        username: values.username,
        password: values.password
      }
    })
      .then(() =>
        NotificationManager.success(
          "Data has been sended!",
          "Successful!",
          2000
        )
      )
      .catch(error => NotificationManager.error(error.message, "ERROR!", 2000));
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Container>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            { required: true, message: "Please input your username!" },
            () => ({
              validator(rule, value = "") {
                let atSign = false;
                let dot = false;
                const regexLower = new RegExp("[a-z]");
                const regexUpper = new RegExp("[A-Z]");

                for (var i = 0; i < value.length; i++) {
                  let char = value.charAt(i);

                  if (char === "-" || regexLower.test(char)) {
                    continue;
                  } else if (char === ".") {
                    dot = true;
                  } else if (char === "@" && i < 5) {
                    return Promise.reject("Minimal letter before @ is 5");
                  } else if (char === "@" && atSign === false) {
                    atSign = true;
                    continue;
                  } else if (char === "@" && atSign === true) {
                    return Promise.reject("Only one @ is correct");
                  } else if (regexUpper.test(char)) {
                    return Promise.reject("Only lowercase latters");
                  } else {
                    return Promise.reject("Incorrect latters!");
                  }
                }
                if (!atSign) {
                  return Promise.reject("@ is required ");
                }
                if (!dot) {
                  return Promise.reject(". is required");
                }
                return Promise.resolve(value);
              }
            })
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
};

export default LoginForm;

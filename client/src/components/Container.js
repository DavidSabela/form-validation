import React from "react";

const Container = ({ children }) => {
  return (
    <div style={{ margin: "auto", width: "400px", paddingTop: "50px" }}>
      {children}
    </div>
  );
};

export default Container;
